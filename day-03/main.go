package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

type checkFunc func(*BitCounts) string

type BitCounts struct {
	Zeroes int
	Ones   int
}

func (bc *BitCounts) addBit(bit string) {
	switch bit {
	case "1":
		bc.Ones++
	case "0":
		bc.Zeroes++
	}
}

func MostFound(bc *BitCounts) string {
	fmt.Printf("Checking zeroes: %d, ones: %d\n", bc.Zeroes, bc.Ones)
	if bc.Zeroes > bc.Ones {
		return "0"
	}
	return "1"
}
func LeastFound(bc *BitCounts) string {
	if bc.Zeroes <= bc.Ones {
		return "0"
	}
	return "1"
}

func (bc BitCounts) String() string {
	return fmt.Sprintf("{Zero:%d, One:%d}", bc.Zeroes, bc.Ones)
}

func readInputFile() (finalArray []string) {
	fileName := "./input.txt"
	if len(os.Args) > 1 {
		fileName = os.Args[1]
	}
	fmt.Println("Opening ", fileName)
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println("ERROR")
		fmt.Println(err)
		panic(err)
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		// This loops over the input file per line
		text := scanner.Text()

		// intValue, _ := strconv.Atoi(text)
		finalArray = append(finalArray, text)
	}
	return

}

func countValues(valuesArray []string) map[int]*BitCounts {
	bitCounts := make(map[int]*BitCounts, 12)
	for _, bitString := range valuesArray {
		bitStringLength := len(bitString)
		for idx, bit := range bitString {
			bitValue := string(bit)
			mapKey := bitStringLength - idx - 1
			if _, ok := bitCounts[mapKey]; !ok {
				bitCounts[mapKey] = &BitCounts{}
			}
			bitCounts[mapKey].addBit(bitValue)
		}
	}
	return bitCounts
}

func getMostOrLeast(bitCountMap map[int]*BitCounts, ourFunc checkFunc) (finalString string) {

	var sortedKeys []int
	for k, _ := range bitCountMap {
		sortedKeys = append(sortedKeys, k)
	}
	sort.Ints(sortedKeys)

	for _, key := range sortedKeys {
		fmt.Printf("index bit: %d, value: %v\n", key, bitCountMap[key])
		finalString = ourFunc(bitCountMap[key]) + finalString
	}
	return
}

func filterValuesByBit(startArray []string, filterIndex int, filterValue string) (finalArray []string) {
	for _, valueString := range startArray {
		if valueString[filterIndex] == filterValue[0] {
			finalArray = append(finalArray, valueString)
		}
	}
	return
}

func calculatePart2(valuesArray []string, ourFunc checkFunc) string {
	// Need to countValues for all results first
	// Then filter based off of largest index value (11) by .MostFound() or LeastFound() depending on O2 vs CO2 rating
	// Run countValues again on filtered array
	// repeat filter but for next index with the same filter type on the next index (10)
	// repeat until one value found.

	finalArray := valuesArray
	maxLength := len(valuesArray[0]) - 1
	indexStart := maxLength
	// Find most O2 generator rating
	for len(finalArray) > 1 && indexStart >= 0 {
		bitCounts := countValues(finalArray)
		filterValue := ourFunc(bitCounts[indexStart])
		finalArray = filterValuesByBit(finalArray, maxLength-indexStart, filterValue)
		indexStart--
	}
	fmt.Println(finalArray)
	return finalArray[0]
}

func main() {
	valuesArray := readInputFile()
	o2Rating := calculatePart2(valuesArray, MostFound)
	o2RatingInt, _ := strconv.ParseInt(o2Rating, 2, 30)
	fmt.Printf("O2 generator rating: %d\n", o2RatingInt)
	co2Rating := calculatePart2(valuesArray, LeastFound)
	co2RatingInt, _ := strconv.ParseInt(co2Rating, 2, 30)
	// leastValue, _ := strconv.ParseInt(getMostOrLeast(bitCounts, LeastFound), 2, 16)
	fmt.Printf("Co2 Scrubber rating: %d\n", co2RatingInt)

	fmt.Println(co2RatingInt * o2RatingInt)
}

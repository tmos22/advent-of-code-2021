package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/*
This file is a template to read in the input files
*/

func readInputFile() (finalArray []string) {
	fileName := "./sample.txt"
	if len(os.Args) > 1 {
		fileName = os.Args[1]
	}
	fmt.Println("Opening ", fileName)
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println("ERROR")
		fmt.Println(err)
		panic(err)
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		// This loops over the input file per line
		text := scanner.Text()

		// intValue, _ := strconv.Atoi(text)
		finalArray = append(finalArray, strings.TrimSpace(text))
	}
	return

}

func main() {
	readInputFile()
}

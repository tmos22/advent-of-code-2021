package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func createIntArray() (finalArray []int) {
	file, err := os.Open("./input.txt")
	if err != nil {
		fmt.Println("ERROR")
		fmt.Println(err)
		panic(err)
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		text := scanner.Text()
		intValue, _ := strconv.Atoi(text)
		finalArray = append(finalArray, intValue)
	}
	return

}

func findIncreases(valuesArray []int) (increases int) {
	increases = 0

	lastValue := -1
	var counter int
	for idx, currentValue := range valuesArray {
		counter = idx
		if idx != 0 {
			if currentValue > lastValue {
				increases++
				fmt.Printf("%d (increased)\n", currentValue)
			} else {
				fmt.Printf("%d (decreased)\n", currentValue)
			}
		} else {
			fmt.Printf("%d (N/A - no previous measurement)\n", currentValue)
		}
		lastValue = currentValue
	}
	fmt.Printf("%d total increases out of %d values\n", increases, counter)
	return
}

func calculateArraySum(slice3Array []int) (sum int) {
	sum = 0
	for _, value := range slice3Array {
		sum += value
	}
	return
}

func generate3SumsArray(valuesArray []int) (sumsArray []int) {
	arrayLength := len(valuesArray)
	for idx, _ := range valuesArray {
		if (idx + 2) >= arrayLength {
			return
		}
		sumsArray = append(sumsArray, calculateArraySum(valuesArray[idx:idx+3]))
	}
	return
}

func main() {
	valuesArray := createIntArray()
	sumsArray := generate3SumsArray(valuesArray)

	findIncreases(sumsArray)
}

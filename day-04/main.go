package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var M1 *regexp.Regexp

type Board [5][5]string

type BingoBoard struct {
	Board            Board
	lastCalledString string
	Winner           bool
	Loser            bool
}

func (bb *BingoBoard) String() string {
	var output string
	for _, row := range bb.Board {
		output += fmt.Sprintf("%s\n", strings.Join(row[:], " "))
	}
	return output
}
func (bb *BingoBoard) LastCalled() int {
	called, _ := strconv.Atoi(bb.lastCalledString)
	return called
}
func (bb *BingoBoard) MarkDigit(drawn string) {
	foundMatch := false
	for rIdx, row := range bb.Board {
		if foundMatch {
			break
		}
		for cIdx, col := range row {
			if col == drawn {
				bb.Board[rIdx][cIdx] = "X"
				bb.lastCalledString = drawn
				foundMatch = true
				break
			}
		}
	}
}

func (bb *BingoBoard) SumRemainder() (finalSum int) {
	finalSum = 0
	for _, row := range bb.Board {
		for _, col := range row {
			if col == "X" {
				continue
			}
			colInt, _ := strconv.Atoi(col)
			finalSum += colInt
		}
	}
	return
}

func (bb *BingoBoard) CheckWin() bool {
	bb.Winner = bb.checkColumns() || bb.checkRows()
	if bb.Winner {
		bb.Loser = false
	}
	return bb.Winner

}

func (bb *BingoBoard) checkRows() bool {
	for _, row := range bb.Board {
		if strings.Join(row[:], "") == "XXXXX" {
			return true
		}
	}
	return false
}

func (bb *BingoBoard) checkColumns() bool {
	for _, column := range bb.Board.Columns() {
		if strings.Join(column[:], "") == "XXXXX" {
			return true
		}
	}
	return false
}

func (b *Board) Columns() [][]string {
	// Swaps the order so the inner arrays are vertical values of a single column
	columnCount := len(b[0])
	rowCount := len(b)
	columns := make([][]string, columnCount)
	for i := range columns {
		columns[i] = make([]string, rowCount)
	}
	i := 0
	for i < columnCount {
		j := 0
		for j < rowCount {
			columns[i][j] = b[j][i]
			j++
		}
		i++
	}
	return columns

}

func parseBingoBoard(rawStringArrays []string) (bb BingoBoard) {
	for rowIdx, row := range rawStringArrays {
		for colIdx, column := range strings.Split(M1.ReplaceAllString(row, " "), " ") {
			bb.Board[rowIdx][colIdx] = column
		}
	}
	bb.Winner = false
	bb.Loser = true
	return
}

func parseInputFile(fileArray []string) (drawNumbers []string, boards []*BingoBoard) {
	drawNumbers = strings.Split(fileArray[0], ",")
	nextStartIdx := 0
	for idx, data := range fileArray[1:] {
		if data == "" {
			nextStartIdx = idx + 1
			continue
		}
		if idx < nextStartIdx {
			continue
		}
		newBoard := parseBingoBoard(fileArray[idx+1 : idx+6])
		boards = append(boards, &newBoard)
		nextStartIdx = idx + 5
	}
	return
}

func readInputFile() (finalArray []string) {
	fileName := "./sample.txt"
	if len(os.Args) > 1 {
		fileName = os.Args[1]
	}
	fmt.Println("Opening ", fileName)
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println("ERROR")
		fmt.Println(err)
		panic(err)
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		// This loops over the input file per line
		text := scanner.Text()

		// intValue, _ := strconv.Atoi(text)
		finalArray = append(finalArray, strings.TrimSpace(text))
	}
	return

}

func playBingo(drawNumbers []string, bingo []*BingoBoard) {
	for _, drawn := range drawNumbers {
		loserCount := 0
		fmt.Printf("Checking %v\n", drawn)
		for _, board := range bingo {
			board.MarkDigit(drawn)
			if board.Winner || board.CheckWin() {
				continue
			}
			loserCount++
		}
		if loserCount == 1 {
			break
		}
	}
	return
}

func main() {
	M1 = regexp.MustCompile(`  `)
	fileInput := readInputFile()
	drawnNumbers, bingoBoards := parseInputFile(fileInput)
	playBingo(drawnNumbers, bingoBoards)
	for idx, board := range bingoBoards {
		if board.Loser {
			for _, drawn := range drawnNumbers {
				board.MarkDigit(drawn)
				if board.CheckWin() {
					break
				}
			}
			winnerPhrase := fmt.Sprintf(" is a LOSER!\n  Final sum: %d\n  Last Called: %d\n   Math: %d", board.SumRemainder(), board.LastCalled(), board.SumRemainder()*board.LastCalled())
			fmt.Printf("Board %d%s\n%v\n\n", idx, winnerPhrase, board)
		}
	}

}

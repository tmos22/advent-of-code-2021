package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const MinnowSpawnTimer int = 8
const AdultSpawnTimer int = 6

type LanternFish struct {
	SpawnTimer int
	Minnow     bool
}

func (lf *LanternFish) CheckSpawn() bool {
	lf.SpawnTimer--
	if lf.SpawnTimer < 0 {
		lf.SpawnTimer = AdultSpawnTimer
		lf.Minnow = false
		return true
	}
	return false
}

func createFish(startingTimer int) *LanternFish {
	return &LanternFish{SpawnTimer: startingTimer, Minnow: false}
}

func spawnFish() *LanternFish {
	return &LanternFish{SpawnTimer: MinnowSpawnTimer, Minnow: true}
}

func readInputFile() (finalArray []*LanternFish) {
	fileName := "./sample.txt"
	if len(os.Args) > 1 {
		fileName = os.Args[1]
	}
	fmt.Println("Opening ", fileName)
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println("ERROR")
		fmt.Println(err)
		panic(err)
	}

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		// This loops over the input file per line
		text := scanner.Text()

		// intValue, _ := strconv.Atoi(text)
		for _, data := range strings.Split(strings.TrimSpace(text), ",") {
			startTimer, _ := strconv.Atoi(data)
			finalArray = append(finalArray, createFish(startTimer))
		}
	}
	return

}

func runSimulation(numberOfDays int, fishes []*LanternFish) (finalCount int) {
	for dayCount := 1; dayCount <= numberOfDays; dayCount++ {
		fmt.Printf("\nDay %d: ", dayCount)
		for _, fish := range fishes {
			if fish.CheckSpawn() {
				fishes = append(fishes, spawnFish())
			}
		}
		for _, fish := range fishes {
			fmt.Printf("%d, ", fish.SpawnTimer)
		}
	}
	finalCount = len(fishes)
	return finalCount
}

func main() {
	startingFish := readInputFile()
	finalCount := runSimulation(256, startingFish)
	fmt.Printf("\nFinal coutn: %d\n", finalCount)
}

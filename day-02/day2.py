#!/bin/env python

import sys
import os


file_name = sys.argv[1]

travelled = {"forward": 0, "depth": 0, "aim": 0}
multipliers = {"up": -1, "down": 1}
trigger_word = "forward"


with open(file_name, "r+") as f:
    for line in f:
        data = line.strip().split(" ")
        if data[0] != trigger_word:
            travelled["aim"] += int(data[1]) * multipliers[data[0]]
            continue

        forward_distance = int(data[1])
        travelled[data[0]] += forward_distance
        depth_change = travelled["aim"] * forward_distance
        travelled["depth"] += depth_change
        print(
            f"Aim was {travelled['aim']}, forward was {forward_distance}, "
            f"Change in depth was {depth_change} to new {travelled['depth']}"
        )

horizontal_distance = travelled["forward"]
vertical_distance = travelled["depth"]

print(
    f"\nHorizontal distance: {horizontal_distance}\n"
    f"Depth:  {vertical_distance}\n"
    f"Total distance: {horizontal_distance*vertical_distance}"
)

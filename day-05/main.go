package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Coordinates struct {
	X int
	Y int
}

func (c *Coordinates) String() string {
	return fmt.Sprintf("(%d,%d)", c.X, c.Y)
}

type Vector struct {
	Initial Coordinates
	Final   Coordinates
}

func (v *Vector) String() string {
	return fmt.Sprintf("%v -> %v", &v.Initial, &v.Final)
}

func (v *Vector) CheckHorizontal() bool {
	if v.Initial.X == v.Final.X {
		return true
	}
	return false
}

func (v *Vector) CheckVertical() bool {
	if v.Initial.Y == v.Final.Y {
		return true
	}
	return false
}

func (v *Vector) PointsCovered() (coords []Coordinates) {
	xCoords := incrementer(v.Initial.X, v.Final.X)
	yCoords := incrementer(v.Initial.Y, v.Final.Y)
	switch {
	case len(xCoords) == len(yCoords):
		for idx := range xCoords {
			coords = append(coords, Coordinates{X: xCoords[idx], Y: yCoords[idx]})
		}
	case v.CheckHorizontal():
		for _, y := range yCoords {
			coords = append(coords, Coordinates{X: xCoords[0], Y: y})
		}
	case v.CheckVertical():
		for _, x := range xCoords {
			coords = append(coords, Coordinates{X: x, Y: yCoords[0]})
		}
	}
	return coords
}

type Increment func(*int)

func inc(counter *int) {
	*counter = *counter + 1
}

func dec(counter *int) {
	*counter = *counter - 1
}

func incrementer(start, finish int) (output []int) {
	incFunc := inc
	if start > finish {
		incFunc = dec
	}
	for counter := start; counter != finish; incFunc(&counter) {
		output = append(output, counter)
	}
	output = append(output, finish)
	return
}

func InitializeVector(coordinates []string) *Vector {
	var v Vector
	var coordInts []int
	for _, points := range coordinates {
		poInt, _ := strconv.Atoi(points)
		coordInts = append(coordInts, poInt)
	}
	v.Initial = Coordinates{X: coordInts[0], Y: coordInts[1]}
	v.Final = Coordinates{X: coordInts[2], Y: coordInts[3]}
	return &v
}

func parseInputFile(fileArray []string) (vectors []*Vector) {
	m1 := regexp.MustCompile(`(\d+),(\d+) -> (\d+),(\d+)`)
	for _, text := range fileArray {
		matches := m1.FindStringSubmatch(text)
		vectors = append(vectors, InitializeVector(matches[1:]))
	}
	return
}

func readInputFile() (finalArray []string) {
	fileName := "./sample.txt"
	if len(os.Args) > 1 {
		fileName = os.Args[1]
	}
	fmt.Println("Opening ", fileName)
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println("ERROR")
		fmt.Println(err)
		panic(err)
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		// This loops over the input file per line
		text := scanner.Text()

		// intValue, _ := strconv.Atoi(text)
		finalArray = append(finalArray, strings.TrimSpace(text))
	}
	return

}

func calculatePart1(vectors []*Vector) map[string]int {
	coveredPoints := make(map[string]int, len(vectors))
	for _, v := range vectors {
		for _, point := range v.PointsCovered() {
			pointString := fmt.Sprintf("%v", &point)
			if _, ok := coveredPoints[pointString]; !ok {
				coveredPoints[pointString] = 1
				continue
			}
			coveredPoints[pointString]++
		}
	}
	return coveredPoints
}

func main() {
	fileInput := readInputFile()
	vectors := parseInputFile(fileInput)
	coveredPoints := calculatePart1(vectors)
	count := 0
	for k, v := range coveredPoints {
		if v > 1 {
			count++
			fmt.Println(k)
		}
	}
	fmt.Printf("Total with overlap: %d\n", count)
}
